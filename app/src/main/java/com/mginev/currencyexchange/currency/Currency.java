package com.mginev.currencyexchange.currency;


public class Currency {
    public String sign(String currency) {
        String[] signs = {"£", "€", "$"};
        if (currency.equals("GBP")) return signs[0];
        if (currency.equals("EUR")) return signs[1];
        if (currency.equals("USD")) return signs[2];

        return "";
    }
}
