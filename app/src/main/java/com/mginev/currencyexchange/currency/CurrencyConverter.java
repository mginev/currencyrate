package com.mginev.currencyexchange.currency;


import java.math.BigDecimal;
import java.util.Map;

public class CurrencyConverter {
    public static BigDecimal getRate(String currency1, String currency2, Map<String, BigDecimal> rates) {
        if (rates == null) return BigDecimal.ZERO;

        BigDecimal c1rate = rates.get(currency1);
        if (c1rate == null) c1rate = BigDecimal.ONE;

        BigDecimal c2rate = rates.get(currency2);
        if (c2rate == null) c2rate = BigDecimal.ONE;

        return c2rate.divide(c1rate, 4, BigDecimal.ROUND_CEILING);
    }

}
