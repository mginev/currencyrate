package com.mginev.currencyexchange.rates;

import android.os.Handler;

import com.mginev.currencyexchange.Config;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

class RatesJob {

    private final Executor executorSingle = Executors.newSingleThreadExecutor();
    private Handler handler = new Handler();
    private RatesUpdater.RatesCallback callback;

    RatesJob(RatesUpdater.RatesCallback callback) {
        this.callback = callback;
    }

    void start() {
        job();
    }

    void stop() {
        handler.removeCallbacksAndMessages(null);
    }

    private void job() {
        handler.removeCallbacksAndMessages(null);
        RatesDownloader ratesDownloader = new RatesDownloader(Config.URL_RATES, callback, new RatesParser());
        ratesDownloader.executeOnExecutor(executorSingle);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                job();
            }
        }, Config.RATE_UPDATE_INTERVAL);
    }
}
