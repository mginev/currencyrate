package com.mginev.currencyexchange.rates;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Map;

interface IRatesParser {
    Map<String, BigDecimal> parse(InputStream stream);
}
