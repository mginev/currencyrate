package com.mginev.currencyexchange.rates;

import java.math.BigDecimal;
import java.util.Map;

public interface IRatesChangeListener {
    void OnRatesChanged(Map<String, BigDecimal> rates);
}
