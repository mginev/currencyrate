package com.mginev.currencyexchange.rates;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

class RatesParser implements IRatesParser {
    @Override
    public Map<String, BigDecimal> parse(InputStream stream) {
        Map<String, BigDecimal> res = new HashMap<>();

        try {
            XmlPullParser xmlParser = Xml.newPullParser();

            xmlParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xmlParser.setInput(stream, null);
            xmlParser.nextTag();
            xmlParser.require(XmlPullParser.START_TAG, null, "gesmes:Envelope");

            while (xmlParser.next() != XmlPullParser.END_TAG) {
                if (xmlParser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = xmlParser.getName();
                // Starts by looking for the entry tag
                if (name.equals("Cube")) {
                    readCube1(xmlParser, res);
                } else {
                    skip(xmlParser);
                }
            }

        } catch (Exception e) {

        }
        return res;
    }

    private void readCube1(XmlPullParser xmlParser, Map<String, BigDecimal> res) throws XmlPullParserException, IOException {
        xmlParser.require(XmlPullParser.START_TAG, null, "Cube");
        while (xmlParser.next() != XmlPullParser.END_TAG) {
            if (xmlParser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = xmlParser.getName();
            if (name.equals("Cube")) {
                readCube2(xmlParser, res);
            } else {
                skip(xmlParser);
            }
        }
    }

    private void readCube2(XmlPullParser xmlParser, Map<String, BigDecimal> res) throws XmlPullParserException, IOException {
        xmlParser.require(XmlPullParser.START_TAG, null, "Cube");
        while (xmlParser.next() != XmlPullParser.END_TAG) {
            if (xmlParser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }

            String name = xmlParser.getName();

            if (name.equals("Cube")) {
                readCube3(xmlParser, res);
            } else {
                skip(xmlParser);
            }
        }
    }

    private void readCube3(XmlPullParser xmlParser, Map<String, BigDecimal> res) throws IOException, XmlPullParserException {
        xmlParser.require(XmlPullParser.START_TAG, null, "Cube");

        if (xmlParser.getAttributeCount() == 2) {
            String cr = xmlParser.getAttributeValue(0);
            String rate = xmlParser.getAttributeValue(1);

            res.put(cr, new BigDecimal(rate));
            Log.d("RatesParser", cr + " : " + res.get(cr));
        }

        xmlParser.nextTag();
        xmlParser.require(XmlPullParser.END_TAG, null, "Cube");
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
