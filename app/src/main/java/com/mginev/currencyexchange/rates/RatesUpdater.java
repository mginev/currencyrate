package com.mginev.currencyexchange.rates;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RatesUpdater implements IRatesUpdater {

    interface RatesCallback {
        void onRatesUpdated(Map<String, BigDecimal> newRates);
    }

    private RatesCallback callback = new RatesCallback() {
        @Override
        public void onRatesUpdated(Map<String, BigDecimal> newRates) {
            lastRates = newRates;
            timestamp = System.currentTimeMillis();
            notifyListeners(newRates);
        }
    };

    private RatesJob ratesJob = new RatesJob(callback);
    private Map<String, BigDecimal> lastRates;
    private long timestamp;
    private List<IRatesChangeListener> listeners = new ArrayList<>();

    public RatesUpdater() {

    }

    @Override
    public Map<String, BigDecimal> getLastRates() {
        return lastRates;
    }

    @Override
    public void addRatesChangeListener(IRatesChangeListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeRatesChangeListener(IRatesChangeListener listener) {
        listeners.remove(listener);
    }

    private void notifyListeners(Map<String, BigDecimal> rates) {
        for (IRatesChangeListener l : listeners) {
            l.OnRatesChanged(rates);
        }
    }

    @Override
    public void start() {
        ratesJob.start();
    }

    @Override
    public void stop() {
        ratesJob.stop();
    }
}
