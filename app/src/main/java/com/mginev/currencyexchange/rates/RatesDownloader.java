package com.mginev.currencyexchange.rates;


import android.os.AsyncTask;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

class RatesDownloader extends AsyncTask<Void, Void, Map<String, BigDecimal>> {

    private String ratesUrl;
    private RatesUpdater.RatesCallback callback;
    IRatesParser parser;

    private void downloadUrlToStream(String url, OutputStream output) {
        InputStream input = null;
        BufferedOutputStream out;
        HttpURLConnection connection = null;

        try {
            URL u = new URL(url);
            connection = (HttpURLConnection) u.openConnection();
            connection.connect();
            int bufferSize = 4096;

            input = connection.getInputStream();

            byte data[] = new byte[bufferSize];
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return;
                }
                output.write(data, 0, count);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return;
    }

    public RatesDownloader(String url, RatesUpdater.RatesCallback callback, IRatesParser parser) {
        this.ratesUrl = url;
        this.callback = callback;
        this.parser = parser;
    }

    @Override
    protected Map<String, BigDecimal> doInBackground(Void... voids) {
        ByteArrayOutputStream ratesStream = new ByteArrayOutputStream();
        downloadUrlToStream(ratesUrl, ratesStream);
        RatesParser parser = new RatesParser();
        return parser.parse(new ByteArrayInputStream(ratesStream.toByteArray()));
    }

    @Override
    protected void onPostExecute(Map<String, BigDecimal> result) {
        super.onPostExecute(result);
        if (callback != null && result != null && result.size() > 0) {
            callback.onRatesUpdated(result);
        }
    }
}
