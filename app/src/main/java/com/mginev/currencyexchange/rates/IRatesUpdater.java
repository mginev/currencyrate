package com.mginev.currencyexchange.rates;


import java.math.BigDecimal;
import java.util.Map;

public interface IRatesUpdater {

    Map<String, BigDecimal> getLastRates();

    void start();
    void stop();

    void addRatesChangeListener(IRatesChangeListener listener);
    void removeRatesChangeListener(IRatesChangeListener listener);
}
