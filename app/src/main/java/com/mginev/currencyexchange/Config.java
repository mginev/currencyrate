package com.mginev.currencyexchange;

public class Config {
    public static String URL_RATES = "http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml";
    public static int RATE_UPDATE_INTERVAL = 30*60*1000;
    public static String[] CURRENCIES = {"GBP", "USD", "EUR", "RUB"};
}
