package com.mginev.currencyexchange.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.mginev.currencyexchange.R;

import java.math.BigDecimal;

public class MainActivity extends FragmentActivity implements IMainView
{
    interface EditTextCallback {
        void onTextChanged(CharSequence text);
    }

    MainPresenter presenter;
    TextView headerRate;
    View upper;
    View bottom;
    Spinner upperCurrencies;
    EditText upperAmount;
    TextView upperRate;
    Spinner bottomCurrencies;
    EditText bottomAmount;
    TextView bottomRate;

    TextWatcher watcherUpper;
    TextWatcher watcherBottom;

    EditTextCallback upperAmountChanged = new EditTextCallback() {
        @Override
        public void onTextChanged(CharSequence text) {
            presenter.upperAmountChanged(text);
        }
    };

    EditTextCallback bottomAmountChanged = new EditTextCallback() {
        @Override
        public void onTextChanged(CharSequence text) {
            presenter.bottomAmountChanged(text);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        upper = findViewById(R.id.layout_container_from);
        upperCurrencies = (Spinner) upper.findViewById(R.id.currency_name);
        upperAmount = (EditText) upper.findViewById(R.id.amount);
        upperRate =  (TextView) upper.findViewById(R.id.rate);
        bottom = findViewById(R.id.layout_container_to);
        bottomCurrencies = (Spinner) bottom.findViewById(R.id.currency_name);
        bottomAmount = (EditText) bottom.findViewById(R.id.amount);
        bottomRate = (TextView) bottom.findViewById(R.id.rate);

        presenter = new MainPresenter(this);

        upperCurrencies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                presenter.setFromCurrency(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bottomCurrencies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                presenter.setToCurrency(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        headerRate = (TextView)findViewById(R.id.header_rate);

        watcherUpper = createTextWatcher(upperAmountChanged);
        upperAmount.addTextChangedListener(watcherUpper);
        watcherBottom = createTextWatcher(bottomAmountChanged);
        bottomAmount.addTextChangedListener(watcherBottom);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void setUpperCurrencies(String[] currencies, int currentPos) {
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(this, R.layout.spinner_item, currencies);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        upperCurrencies.setAdapter(adapter);
        upperCurrencies.setSelection(currentPos);
    }

    @Override
    public void setBottomCurrencies(String[] currencies, int currentPos) {
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(this, R.layout.spinner_item, currencies);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bottomCurrencies.setAdapter(adapter);
        bottomCurrencies.setSelection(currentPos);
    }

    @Override
    public BigDecimal getUpperAmount() {
        try {
            BigDecimal amount = new BigDecimal(upperAmount.getText().toString());
            return amount;
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    @Override
    public void setUpperAmount(BigDecimal amount) {
        upperAmount.removeTextChangedListener(watcherUpper);
        upperAmount.setText(amount.toString());
        upperAmount.addTextChangedListener(watcherUpper);
    }

    @Override
    public void setBottomAmount(BigDecimal amount) {
        bottomAmount.removeTextChangedListener(watcherBottom);
        bottomAmount.setText(amount.toString());
        bottomAmount.addTextChangedListener(watcherBottom);
    }

    @Override
    public void setUpperRate(String currency1, String currency2, String amount2) {
        if (headerRate != null) {
            String text = getResources().getString(R.string.upper_rate, currency1, currency2, amount2);
            headerRate.setText(text);
        }
    }

    @Override
    public void hideUpperRate() {
        if (headerRate != null) {
            headerRate.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void showUpperRate() {
        if (headerRate != null) {
            headerRate.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setBottomRate(String currency1, String currency2, String amount2) {
        String text = getResources().getString(R.string.upper_rate, currency1, currency2, amount2);
        bottomRate.setText(text);
    }

    @Override
    public void hideBottomRate() {
        bottomRate.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showBottomRate() {
        bottomRate.setVisibility(View.VISIBLE);
    }


    private TextWatcher createTextWatcher(final EditTextCallback editTextCallback) {

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editTextCallback != null) {
                    editTextCallback.onTextChanged(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        return watcher;
    }
}
