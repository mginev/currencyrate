package com.mginev.currencyexchange.ui;

import android.content.Context;

import java.math.BigDecimal;

interface IMainView {

    void setUpperCurrencies(String[] currencies, int currentPos);
    void setBottomCurrencies(String[] currencies, int currentPos);

    BigDecimal getUpperAmount();
    void setUpperAmount(BigDecimal amount);
    void setBottomAmount(BigDecimal amount);

    void setUpperRate(String currency1, String currency2, String amount2);
    void hideUpperRate();
    void showUpperRate();

    void setBottomRate(String currency1, String currency2, String amount2);
    void hideBottomRate();
    void showBottomRate();

}
