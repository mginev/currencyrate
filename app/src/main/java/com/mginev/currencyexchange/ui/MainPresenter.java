package com.mginev.currencyexchange.ui;

import com.mginev.currencyexchange.Config;
import com.mginev.currencyexchange.currency.CurrencyConverter;
import com.mginev.currencyexchange.currency.Currency;
import com.mginev.currencyexchange.rates.IRatesChangeListener;
import com.mginev.currencyexchange.rates.IRatesUpdater;
import com.mginev.currencyexchange.rates.RatesUpdater;

import java.math.BigDecimal;
import java.util.Map;

class MainPresenter implements IRatesChangeListener {

    private IMainView mainView;
    private String currencyFrom;
    private String currencyTo;

    private Currency currencyList = new Currency();
    private IRatesUpdater ratesUpdater = new RatesUpdater();

    MainPresenter(IMainView mainView) {
        this.mainView = mainView;
        mainView.setUpperCurrencies(Config.CURRENCIES, 0);
        mainView.setBottomCurrencies(Config.CURRENCIES, 1);
        currencyFrom = Config.CURRENCIES[0];
        currencyTo = Config.CURRENCIES[1];

        ratesUpdater.addRatesChangeListener(this);
    }


    void onPause() {
        ratesUpdater.stop();
    }

    void onResume() {
        ratesUpdater.start();
    }

    void setFromCurrency(int num) {
        currencyFrom = Config.CURRENCIES[num];
        updateUpperRate();
        updateBottomRate();
    }

    void setToCurrency(int num) {
        currencyTo = Config.CURRENCIES[num];
        updateUpperRate();
        updateBottomRate();
    }

    void upperAmountChanged(CharSequence amount) {
        try {
            BigDecimal newAmount = new BigDecimal(amount.toString());
            if (newAmount.compareTo(BigDecimal.ZERO) == 0) {
                mainView.setBottomAmount(BigDecimal.ZERO);
                return;
            }
            BigDecimal rate = CurrencyConverter.getRate(currencyFrom, currencyTo, ratesUpdater.getLastRates());
            mainView.setBottomAmount(newAmount.multiply(rate));

        } catch (Exception e) {
            mainView.setBottomAmount(BigDecimal.ZERO);
        }
    }

    void bottomAmountChanged(CharSequence amount) {
        try {
            BigDecimal newAmount = new BigDecimal(amount.toString());

            if (newAmount.compareTo(BigDecimal.ZERO) == 0) {
                mainView.setUpperAmount(BigDecimal.ZERO);
                return;
            }
            BigDecimal rate = CurrencyConverter.getRate(currencyTo, currencyFrom, ratesUpdater.getLastRates());
            BigDecimal newUpperAmount = newAmount.multiply(rate);
            mainView.setUpperAmount(newUpperAmount);

        } catch (Exception e) {
            mainView.setUpperAmount(BigDecimal.ZERO);
        }
    }

    @Override
    public void OnRatesChanged(Map<String, BigDecimal> rates) {
        updateUpperRate();
        updateBottomRate();
    }

    private void updateUpperRate() {
        if (currencyTo.equals(currencyFrom) || ratesUpdater.getLastRates() == null) {
            mainView.hideUpperRate();
        } else {
            mainView.showUpperRate();
            mainView.setUpperRate(currencyList.sign(currencyFrom), currencyList.sign(currencyTo),
                    CurrencyConverter.getRate(currencyFrom, currencyTo, ratesUpdater.getLastRates()).toString());
        }
    }

    private void updateBottomRate() {
        if (currencyTo.equals(currencyFrom) || ratesUpdater.getLastRates() == null) {
            mainView.hideBottomRate();
        } else {
            mainView.showBottomRate();
            mainView.setBottomRate(currencyList.sign(currencyTo), currencyList.sign(currencyFrom),
                    CurrencyConverter.getRate(currencyTo, currencyFrom, ratesUpdater.getLastRates()).toString());
        }
    }

}
